//
//  ViewController.m
//  Whatever
//
//  Created by James Cash on 13-03-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "TouchView.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *coolButton;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.coolButton setTitle:@"HELLO!" forState:UIControlStateNormal];
    [self.coolButton addTarget:self action:@selector(thingy:) forControlEvents:UIControlEventTouchUpInside];

    UIView *view0 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    view0.backgroundColor = [UIColor blueColor];
    [self.view addSubview:view0];

    UIView *view1 = [[UIView alloc] initWithFrame:CGRectMake(10, 10, 300, 300)];
    view1.backgroundColor = [UIColor redColor];
    [self.view addSubview:view1];

    UIView *view2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    view2.backgroundColor = [UIColor darkGrayColor];
    [view1 addSubview:view2];

    TouchView *tv1 = [[TouchView alloc] initWithFrame:CGRectMake(50, 50, 400, 400)];
    tv1.backgroundColor = [UIColor cyanColor];

    TouchView *tv2 = [[TouchView alloc] initWithFrame:CGRectMake(50, 50, 200, 200)];
    tv2.backgroundColor = [UIColor greenColor];
    
    [tv1 addSubview:tv2];
    [self.view addSubview:tv1];

}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    NSLog(@"Touches in View controller; next responder is %@", self.nextResponder);
}

- (IBAction)buttonPress:(id)sender {
    NSLog(@"Button pressed");
}


- (IBAction)thingy:(id)sender {
    NSLog(@"Stuff");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
