//
//  TouchView.m
//  Whatever
//
//  Created by James Cash on 13-03-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "TouchView.h"

@implementation TouchView

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    NSLog(@"Touch in TouchView %@; nextResponder %@", self, self.nextResponder);    
    [self.nextResponder touchesBegan:touches withEvent:event];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
